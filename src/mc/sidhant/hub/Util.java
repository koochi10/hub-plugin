package mc.sidhant.hub;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class Util {
	//use a set instead of a list because set has a O(1) time for checking if it contains a value
	public static Set<UUID> inLobby;
	
	public static String hideString = ChatColor.MAGIC+"123"+ChatColor.BLUE+" Toggle Players! "+ChatColor.WHITE+ChatColor.MAGIC+"123";
	public static String selectorString = ChatColor.MAGIC+"123"+ChatColor.BLUE+" Select a Server! "+ChatColor.WHITE+ChatColor.MAGIC+"123";
	
	Util() {
		inLobby = new HashSet<UUID>();
	}
	//we can use this method to change player attributes when they respawn join game or join lobby.
	public static void setPlayerInv(Player p) {
		PlayerInventory inv = p.getInventory();
		//clear inventory so we can safley call this method when needed
		inv.clear();
		//adds the hidePlayer slimeball item to position 4
		inv.setItem(4, setItemNameAndLore(new ItemStack(Material.SLIME_BALL), hideString, null));
		//adds the serverSelector item to position 0
		inv.setItem(0, setItemNameAndLore(new ItemStack(Material.COMPASS), selectorString, null));
		//max health is changeable in bukkit code
		p.setHealth(p.getMaxHealth());
		//set food to max mc value which is 20
		p.setFoodLevel(20);
		//need to allow flight for double jump effect look at double jump code
		p.setAllowFlight(true);
	}
	
	public static void addLobby(Player p) {
		inLobby.add(p.getUniqueId());
	}
	public static void removeLobby(Player p) {
		inLobby.remove(p.getUniqueId());
	}
	//checks if a player is in the lobby
	public static Boolean hasLobby(Player p) {
		if(inLobby.contains(p.getUniqueId())) return true;
		return false;
	}
	
	//use as an easy way in all classes to manipulate name and lore
    public static ItemStack setItemNameAndLore(ItemStack item, String name, String[] lore) {
        ItemMeta im = item.getItemMeta();
        if(name!= null) im.setDisplayName(name);
        if(lore != null) im.setLore(Arrays.asList(lore));
        item.setItemMeta(im);
        return item;
    }

}
