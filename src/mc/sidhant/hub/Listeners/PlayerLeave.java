package mc.sidhant.hub.Listeners;

import mc.sidhant.hub.Util;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerLeave implements Listener {
	@EventHandler
	public void leave(PlayerQuitEvent e) {
		//checks for in Lobby return false
		Util.removeLobby(e.getPlayer());
		//cancels the quit message
		e.setQuitMessage(null);
	}
}
