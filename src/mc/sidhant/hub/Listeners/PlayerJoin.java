package mc.sidhant.hub.Listeners;

import java.util.Random;

import mc.sidhant.hub.Main;
import mc.sidhant.hub.Util;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class PlayerJoin implements Listener {
	@EventHandler
	public void join(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		// allows checks for inLobby
		Util.addLobby(p);
		//set Inventory
		Util.setPlayerInv(p);
		//removes join message
		e.setJoinMessage(null);
		//Spawn fireworks at the players location
		spawnFireworks(p);
	}
	
	private void spawnFireworks(final Player p) {
		final Location loc = p.getEyeLocation();
		 new BukkitRunnable() {
	            int x = 0;
	            public void run() {
	            	x++;
	            	//shoot 5 fireworks magic numbers I KNOW.
	            	if(x >= 5){
	                    this.cancel();
	                } else {
	                	if(p != null) {
	                		shootFirework(loc);
	                	}
	                }
	            }
	        }.runTaskTimer(Main.getPlugin(), 0, 10);
	}
	
	//use of protected when accessing from a different thread
	protected void shootFirework(Location loc) {
		Firework fw = (Firework) loc.getWorld().spawn(loc,
				Firework.class);
		FireworkMeta fwm = fw.getFireworkMeta();

		// Our random generator
		Random r = new Random();

		// Get the type
		int rt = r.nextInt(4) + 1;
		Type type = Type.BALL;
		if (rt == 1)
			type = Type.BALL;
		if (rt == 2)
			type = Type.BALL_LARGE;
		if (rt == 3)
			type = Type.BURST;
		if (rt == 4)
			type = Type.CREEPER;
		if (rt == 5)
			type = Type.STAR;

		// Get our random colours
		int r1i = r.nextInt(17) + 1;
		int r2i = r.nextInt(17) + 1;
		Color c1 = getColor(r1i);
		Color c2 = getColor(r2i);

		// Create our effect with this
		FireworkEffect effect = FireworkEffect.builder()
				.flicker(r.nextBoolean()).withColor(c1).withFade(c2)
				.with(type).trail(r.nextBoolean()).build();

		// Then apply the effect to the meta
		fwm.addEffect(effect);

		// Generate some random power and set it
		int rp = r.nextInt(2) + 1;
		fwm.setPower(rp);

		// Then apply this to our rocket
		fw.setFireworkMeta(fwm);

		// use meta to customize the firework or add parameters to the
		// method
		// fw.setVelocity(p.getLocation().getDirection().multiply(speed);
		// speed is how fast the firework flies
	}
	private Color getColor(int i) {
		Color c = null;
		if (i == 1) {
			c = Color.AQUA;
		}
		if (i == 2) {
			c = Color.BLACK;
		}
		if (i == 3) {
			c = Color.BLUE;
		}
		if (i == 4) {
			c = Color.FUCHSIA;
		}
		if (i == 5) {
			c = Color.GRAY;
		}
		if (i == 6) {
			c = Color.GREEN;
		}
		if (i == 7) {
			c = Color.LIME;
		}
		if (i == 8) {
			c = Color.MAROON;
		}
		if (i == 9) {
			c = Color.NAVY;
		}
		if (i == 10) {
			c = Color.OLIVE;
		}
		if (i == 11) {
			c = Color.ORANGE;
		}
		if (i == 12) {
			c = Color.PURPLE;
		}
		if (i == 13) {
			c = Color.RED;
		}
		if (i == 14) {
			c = Color.SILVER;
		}
		if (i == 15) {
			c = Color.TEAL;
		}
		if (i == 16) {
			c = Color.WHITE;
		}
		if (i == 17) {
			c = Color.YELLOW;
		}

		return c;
	}
	
}
