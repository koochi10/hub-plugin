package mc.sidhant.hub.Listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

public class MobKill implements Listener {
	@EventHandler
	public void spawn(CreatureSpawnEvent e) {
		SpawnReason reason = e.getSpawnReason();
		//don't allows mobs to spawn unless they spawn from a spawner or a custom plugin
		if(reason != SpawnReason.CUSTOM || reason != SpawnReason.SPAWNER) {
			e.setCancelled(true);
		}
	}
}
