package mc.sidhant.hub.Listeners;

import mc.sidhant.hub.Util;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CompassController implements Listener {
	//size of newly created inventory
	// inventory size must be divisible by 9
	public int INVENTORY_SIZE = 36;
	// use display names as identifiers for items. I can do this other ways too but in my opinion its the easiest.
	public static String SKELETON_REALM = "Skeleton Realm";
	public static String BLAZE_REALM = ChatColor.YELLOW+"Blaze Realm";
	public static String ZOMBIE_REALM = ChatColor.GREEN+"Zombie Realm";
	public static String MAGMA_REALM = ChatColor.RED+"Magma Realm";
	
	@EventHandler
	public void playerInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		//checks if a player is contained in the lobby
		if(Util.hasLobby(p)) {
			Action ac = e.getAction();
			//checks if the action contains certain UI elements
			if(ac == Action.RIGHT_CLICK_AIR || ac == Action.RIGHT_CLICK_BLOCK) {
				ItemStack item = p.getItemInHand();
				if(item == null) return;
				ItemMeta meta = item.getItemMeta();
				if(meta == null) return;
				String name = meta.getDisplayName();
				if(name == null) return;
				//checks if the block in hand matches the same string
				if(name == Util.selectorString) {
					//creates the inventory to use
					Inventory inv = Bukkit.createInventory(p, INVENTORY_SIZE, "Server Selector");
					for(int x = 0; x<INVENTORY_SIZE; x++) {
						// I tried so hard to find glass pane under the materials list but I can't find it anywhere lol
						inv.setItem(x, new ItemStack(Material.GLASS));
					}
					//lore for Skeleton Realm
					String[] skeleton = {ChatColor.GRAY+"HeadHunting "+ChatColor.WHITE+"Skeleton "+ChatColor.GRAY+"Realm", ChatColor.GREEN+"Played by "+ChatColor.BOLD+"CreepersEdge"};
					//lore for Blaze Realm
					String[] blaze = {ChatColor.GRAY+"HeadHunting "+ChatColor.YELLOW+"Blaze "+ChatColor.GRAY+"Realm", ChatColor.GREEN+"Played by "+ChatColor.YELLOW+ChatColor.BOLD+"OhTekkers"};
					//lore for Zombie Realm
					String[] zombie = {ChatColor.GRAY+"HeadHunting "+ChatColor.GREEN+"Zombie "+ChatColor.GRAY+"Realm", ChatColor.YELLOW+"This server runs "+ChatColor.YELLOW+"McMMO!"};
					//lore for magma Realm
					String[] magma = {ChatColor.GRAY+"HeadHunting "+ChatColor.RED+"Magma "+ChatColor.GRAY+"Realm", ChatColor.YELLOW+"This server runs "+ChatColor.YELLOW+"McMMO!"};
					
					//adds gui for Skeleton Realm
					inv.setItem(12, Util.setItemNameAndLore(new ItemStack(Material.DIAMOND_SWORD), SKELETON_REALM, skeleton));
					inv.setItem(14, Util.setItemNameAndLore(new ItemStack(Material.GOLD_SWORD), BLAZE_REALM, blaze));
					inv.setItem(21, Util.setItemNameAndLore(new ItemStack(Material.IRON_SWORD), ZOMBIE_REALM, zombie));
					inv.setItem(23, Util.setItemNameAndLore(new ItemStack(Material.STONE_SWORD), MAGMA_REALM, magma));
					
					//give the player this inventory
					p.openInventory(inv);
				}
			}
		}
	}
}
