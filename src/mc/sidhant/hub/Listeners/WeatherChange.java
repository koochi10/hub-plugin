package mc.sidhant.hub.Listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WeatherChange implements Listener {
	/*
	 * I always try get rid of rain whenever possible because a lot of the
	 * time rain causes client side lag assume that weather is non-raining at start
	 */
	@EventHandler
	public void stoprain(WeatherChangeEvent e) {
		//if rain trys to fall
		if(e.toWeatherState()) {
			//cancel it
			e.setCancelled(true);
		}
	}
}
