package mc.sidhant.hub.Listeners;

import mc.sidhant.hub.Main;
import mc.sidhant.hub.Util;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.util.Vector;

public class DoubleJump implements Listener {
	/*
	 * A double jump class that does not rely on the PlayerMoveEvent to help
	 * ease server load
	 */
	// time in seconds to cooldown the flight toggle effect
	public int FLIGHT_COOLDOWN = 3;
	@EventHandler
	public void doublejump(PlayerToggleFlightEvent e) {
		final Player p = e.getPlayer();
		// checks if the player is in the lobby
		if (Util.hasLobby(p)) {
			// cancels flight event
			e.setCancelled(true);
			jump(p);
			p.setAllowFlight(false);
			Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getPlugin(), new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// since it is a delayed task the player could have logged off
					if(p.isOnline()) {
						// allows from flight again
						p.setAllowFlight(true);
					}
				}	
			}, 20L * FLIGHT_COOLDOWN);
		}
	}

	// shoots the player forward when they jump
	private void jump(Player p) {
		// TODO Auto-generated method stub
		Vector vec = p.getLocation().getDirection();
		p.setVelocity(new Vector(vec.multiply(1.5).getX(), vec.multiply(1)
				.getY() + .7, vec.multiply(1.5).getZ()));
		p.playSound(p.getLocation(), Sound.NOTE_SNARE_DRUM, 1F, 1F);
	}
}
