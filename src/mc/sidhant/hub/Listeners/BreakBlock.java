package mc.sidhant.hub.Listeners;

import mc.sidhant.hub.Util;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BreakBlock implements Listener {
	@EventHandler
	public void broke(BlockBreakEvent e) {
		Player p = e.getPlayer();
		if(Util.hasLobby(p)) {
			e.setCancelled(true);
		}
	}
}
