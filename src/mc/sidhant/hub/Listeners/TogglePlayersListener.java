package mc.sidhant.hub.Listeners;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import mc.sidhant.hub.Util;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.meta.ItemMeta;

public class TogglePlayersListener implements Listener {
	/*
	 * This class is used to check and perform a task when a player toggles
	 * their show players item
	 */
	//use a set instead of an array to save computation power in this case
	Set<UUID> hiding;

	public TogglePlayersListener() {
		hiding = new HashSet<UUID>();
	}

	@EventHandler
	public void playerInteract(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		// checks if a player is contained in the lobby
		if (Util.hasLobby(p)) {
			Action ac = e.getAction();
			// checks if the action contains certain UI elements
			if (ac == Action.RIGHT_CLICK_AIR || ac == Action.RIGHT_CLICK_BLOCK) {
				ItemMeta meta = p.getItemInHand().getItemMeta();
				if(meta == null) return;
				String name = meta.getDisplayName();
				if(name == null) return;
 				//checks if the block in hand matches the same string
				if (name == Util.hideString) {
					//gets the id of the player
					UUID id = p.getUniqueId();
					// if player has already pre specified that they are hiding players
					if(hiding.contains(id)) {
						//toggle to give players vision
						for(Player others: Bukkit.getOnlinePlayers()) {
							hiding.remove(id);
							p.showPlayer(others);
						}
						p.sendMessage(ChatColor.GREEN+"Now showing players");
					}
					//if the player is not in the hiding list
					else {
						for(Player others: Bukkit.getOnlinePlayers()) {
							hiding.add(id);
							p.hidePlayer(others);
						}
						p.sendMessage(ChatColor.RED+"Now hiding players");
					}
				}
			}
		}
	}

	// remove player from map if they leave the game to prevent garbage from
	// being collected
	@EventHandler
	public void playerLeave(PlayerQuitEvent e) {
		hiding.remove(e.getPlayer().getUniqueId());
	}
	//when a player logs in players who have hide selected shouldn't be able to see them
	@EventHandler
	public void playerlogin(PlayerLoginEvent e) {
		Player p = e.getPlayer();
		//iterate through set
		for(UUID otherids: hiding) {
			// for each player wanting to hide, hide the newly logged in player
			Bukkit.getPlayer(otherids).hidePlayer(p);
		}
	}
}
