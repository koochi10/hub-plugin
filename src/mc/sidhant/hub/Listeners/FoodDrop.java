package mc.sidhant.hub.Listeners;

import mc.sidhant.hub.Util;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodDrop implements Listener {
	/*
	 * Cancel food level changes in lobby
	 */
	@EventHandler
	public void drop(FoodLevelChangeEvent e) {
		HumanEntity he = e.getEntity();
		if(he instanceof Player) {
			Player p = (Player) he;
			//checks if player is in the lobby
			if(Util.hasLobby(p)) {
				e.setCancelled(true);
			}
		}
	}
}
