package mc.sidhant.hub.Listeners;

import mc.sidhant.hub.Util;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ServerSelector implements Listener {
	@EventHandler
	public void clicked(InventoryClickEvent e) {
		// make sure it is a player who clicked human entity could result in npc
		// but can a npc really click their inventory? idk just check for saftey lol
		HumanEntity en = e.getWhoClicked();
		if (en instanceof Player) {
			// can now safely cast entity to a player
			Player p = (Player) en;
			// checks if the player is in the lobby
			if (Util.hasLobby(p)) {
				// cancels the event to move objects
				e.setCancelled(true);
				//gets item value
				ItemStack item = e.getCurrentItem();
				//make sure the item is not null;
				if(item == null) return;
				// gets the items meta value
				ItemMeta meta = item.getItemMeta();
				// make sure meta is not null
				if(meta == null) return;
				// matches name of item with realm items
				String name = meta.getDisplayName();
				//make sure the item has a name
				if(name == null) return;
				if (name == CompassController.SKELETON_REALM) {
					// Bungee cord code to connect to skeleton realm
					p.sendMessage(ChatColor.GREEN+"Now connecting to the Skeleton Realm!");
				}
				if (name == CompassController.ZOMBIE_REALM) {
					// Bungee cord code to connect to the zombie realm
					p.sendMessage(ChatColor.GREEN+"Now connecting to the Zombie Realm!");
				}
				if (name == CompassController.BLAZE_REALM) {
					// Bungee cord code to connect to the blaze realm
					p.sendMessage(ChatColor.GREEN+"Now connecting to the Blaze Realm!");
				}
				if (name == CompassController.MAGMA_REALM) {
					// Bungee cord code to connect ot the magma realm
					p.sendMessage(ChatColor.GREEN+"Now connecting to the Magma Realm!");
				}
			}
		}
	}
	@EventHandler
	public void login(PlayerLoginEvent e) {
		
	}
}
