package mc.sidhant.hub.Listeners;

import mc.sidhant.hub.Util;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class Damage implements Listener {
	/*
	 * Negates all types of damage in lobby except if player falls into void
	 * then teleports them back into spawn
	 */
	@EventHandler
	public void damaged(EntityDamageEvent e) {
		Entity en = e.getEntity();
		// checks if the entity being damaged is indeed a player
		if (en instanceof Player) {
			Player p = (Player) en;
			// checks if the player is in lobby
			if (Util.hasLobby(p)) {
				// if the player takes void damage teleport them back to the
				// spawn
				if (e.getCause() == DamageCause.VOID) {
					p.teleport(p.getWorld().getSpawnLocation());
				}
				// just cancel all other types of damage
				else {
					e.setCancelled(true);
				}
			}
		}
	}
}
