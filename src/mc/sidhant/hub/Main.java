package mc.sidhant.hub;

import mc.sidhant.hub.Listeners.BreakBlock;
import mc.sidhant.hub.Listeners.CompassController;
import mc.sidhant.hub.Listeners.Damage;
import mc.sidhant.hub.Listeners.DoubleJump;
import mc.sidhant.hub.Listeners.FoodDrop;
import mc.sidhant.hub.Listeners.ItemDrop;
import mc.sidhant.hub.Listeners.MobKill;
import mc.sidhant.hub.Listeners.PlayerJoin;
import mc.sidhant.hub.Listeners.PlayerLeave;
import mc.sidhant.hub.Listeners.ServerSelector;
import mc.sidhant.hub.Listeners.TogglePlayersListener;
import mc.sidhant.hub.Listeners.WeatherChange;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{
	private static Plugin plugin;
	@Override
	public void onEnable() {
		getServer().getLogger().info("Hub Plugin activated");
		plugin = this;
		new Util();
		
		//register player controllers
		registerEvents(this, new PlayerJoin(), new PlayerLeave(), new Damage(), new BreakBlock());
		//register UI controllers
		registerEvents(this, new CompassController(), new TogglePlayersListener(), new ServerSelector(), new ItemDrop(), new FoodDrop());
		//register world controllers
		registerEvents(this, new WeatherChange(), new MobKill());
		//register jump event
		registerEvents(this, new DoubleJump());
	}
	
	@Override
	public void onDisable() {
		getServer().getLogger().info("Disabling Hub Plugin");
	}
	
	//makes registering events easier
	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
		for (Listener listener : listeners) {
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
	}
	 
	public static Plugin getPlugin() {
		return plugin;
	}
}
